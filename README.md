# 天空旅者

#### 介绍


玩法和跳一跳/欢乐跳瓶相同，无限关卡，使用Unity5.x（现在升级为2018），所有代码完全由自己编写，项目使用的所有插件也是由自己开发.

**极简UI缓动效果工具** : [EasyAnimation](https://gitee.com/Foldcc/EasyAnimation) 

**组件化UI控制框架** ：[Ice-creamView](https://gitee.com/Foldcc/Ice-creamView)

该项目包含游戏完整源码，注释详细，可直接打包到ios、android、wp、windows等平台，供参考学习。


#### 实际效果

![主页面](ReadmeSources/GIF.gif)

![游戏页面](ReadmeSources/GIF2.gif)

![游戏页面2](ReadmeSources/GIF3.gif)

#### 使用说明

clone项目后打开Unity，点击Open，选择该目录，进入后自己点击play即可运行。


